// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost/xhop-server/api/',
  sessionName: '__alpha_log',
  tempOrderId: 'Slip__Alpha_Z02',
  tokenName: 'atl_map_DES',
  sk: 'sk_test_1aa3a8ae12daab7255be2dec2fc428fd0778993e',
  pk: 'pk_test_bd57277198b482f2e04283aa0c0f202fcbfe7791'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
