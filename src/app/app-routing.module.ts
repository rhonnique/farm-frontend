import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products.service';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { CartsComponent } from './carts/carts.component';
import { DashboardComponent } from './account/dashboard/dashboard.component';
import { LoginComponent } from './account/login/login.component';
import { RegisterComponent } from './account/register/register.component';
import { AuthGuard } from './auth.guard';
import { NotFoundComponent } from './not-found/not-found.component';
import { OrderHistoryComponent } from './account/order-history/order-history.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { AccountComponent } from './account/account/account.component';
import { ForgotPasswordComponent } from './account/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';
import { SummaryService } from './account/dashboard/summary.service';
import { OrdersService } from './account/order-history/orders.service';
import { EditInfoComponent } from './account/edit-info/edit-info.component';
import { CustomerService } from './account/edit-info/customer.service';
import { CheckoutComponent } from './checkout/checkout/checkout.component';
import { DeliveryComponent } from './checkout/delivery/delivery.component';
import { DeliveryService } from './checkout/delivery/delivery.service';
import { NoAuthGuard } from './no.auth.guard';
import { CheckoutService } from './checkout/checkout.service';
import { PaymentComponent } from './checkout/payment/payment.component';
import { DiscountComponent } from './checkout/discount/discount.component';
import { CompleteComponent } from './checkout/complete/complete.component';
import { OrderSummaryService } from './checkout/complete/order-summary.service';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'categories',
    component: CategoriesComponent
  },
  {
    path: 'categories/:id',
    component: CategoriesComponent
  },
  {
    path: 'carts',
    component: CartsComponent
  },
  {
    path: 'products',
    component: ProductsComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      products: ProductsService
    }
  },
  {
    path: 'product/:id',
    component: ProductDetailsComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      products: ProductsService
    }
  },
  {
    path: 'account/login',
    component: LoginComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'account/register',
    component: RegisterComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'account/forgot-password',
    component: ForgotPasswordComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'account/reset-password',
    component: ResetPasswordComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
        runGuardsAndResolvers: 'always',
        resolve: {
          products: SummaryService
        }
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'order-history',
        component: OrderHistoryComponent,
        runGuardsAndResolvers: 'always',
        resolve: {
          products: OrdersService
        }
      },
      {
        path: 'edit-info',
        component: EditInfoComponent,
        runGuardsAndResolvers: 'always',
        resolve: {
          products: CustomerService
        }
      }
    ]
  },

  {
    path: 'checkout',
    component: CheckoutComponent,
    canActivate: [AuthGuard],
    runGuardsAndResolvers: 'always',
    resolve: {
      checkout: CheckoutService
    },
    children: [
      /* {
        path: '',
        redirectTo: 'delivery',
        pathMatch: 'full'
      }, */
      {
        path: '',
        component: DiscountComponent,
        data: {
          default: true
        }
      },
      {
        path: 'delivery',
        component: DeliveryComponent
      },
      {
        path: 'payment',
        component: PaymentComponent
      }
    ]
  },
  {
    path: 'order-complete',
    canActivate: [AuthGuard],
    component: CompleteComponent,
    resolve: {
      checkout: OrderSummaryService
    }
  },


  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
