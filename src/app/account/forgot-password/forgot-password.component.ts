import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelsService } from 'src/app/models.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { MsgService } from 'src/app/msg.service';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  form: FormGroup;
  returnUrl: any;

  constructor(
    private fb: FormBuilder,
    private modelsService: ModelsService,
    private cookieService: CookieService,
    private router: Router,
    private msgService: MsgService,
    private route: ActivatedRoute) {
    const user = modelsService.user;
    if (user) {
      this.router.navigate(['/account']);
    }

    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      email: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  async submit(post) {
    $('#btnForgotPassword').attr('disabled', true);
    await this.modelsService.forgot_password(post)
      .subscribe(response => {
        const { token } = response;
        console.log(response, 'ForgotPassword....');
        this.router.navigate([`/account/reset-password/${token}`]);
      }, (error) => {
        $('#btnForgotPassword').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnForgotPassword').attr('disabled', false);
      });
  }

}
