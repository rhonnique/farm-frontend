import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelsService } from 'src/app/models.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { MsgService } from 'src/app/msg.service';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  returnUrl: any;

  constructor(
    private fb: FormBuilder,
    private modelsService: ModelsService,
    private cookieService: CookieService,
    private router: Router,
    private msgService: MsgService,
    private route: ActivatedRoute) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      current_password: ['', Validators.required],
      new_password: ['', Validators.required],
      confirm_new_password: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  async submit(post) {
    $('#btnChangePassword').attr('disabled', true);
    await this.modelsService.change_password(post)
      .subscribe(response => {
        const { data, token } = response;
        this.modelsService.deleteCookie(environment.tokenName);
        this.modelsService.set_user_data('');
        this.msgService.send({
          type: 'setuserdata',
          user_data: ''
        });
        this.router.navigate(['/account/login']);
      }, (error) => {
        $('#btnChangePassword').attr('disabled', false);
        console.error(error.error, 'Change Password error....');
      }, () => {
        $('#btnChangePassword').attr('disabled', false);
      });
  }

}
