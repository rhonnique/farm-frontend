import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModelsService } from 'src/app/models.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute } from '@angular/router';
import { MsgService } from 'src/app/msg.service';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrl: any;

  constructor(
    private fb: FormBuilder,
    private modelsService: ModelsService,
    private cookieService: CookieService,
    private router: Router,
    private msgService: MsgService,
    private route: ActivatedRoute) {


    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/account';
    if (this.modelsService.user) {
      this.router.navigate(['/account']);
    }
  }

  async login(post) {
    $('#btnLogin').attr('disabled', true);
    await this.modelsService.login(post)
      .subscribe(response => {
        const { data, token } = response;
        this.cookieService.delete(environment.tokenName);
        const expiredDate = new Date();
        expiredDate.setDate(expiredDate.getDate() + 30);
        this.cookieService.set(environment.tokenName, token, expiredDate, '/');
        this.modelsService.set_user_data(data);
        this.msgService.send({
          type: 'setuserdata',
          user_data: data
        });
        this.router.navigate([this.returnUrl]);
      }, (error) => {
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnLogin').attr('disabled', false);
      });
  }

}
