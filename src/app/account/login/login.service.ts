import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, RoutesRecognized } from '@angular/router';
import { ModelsService } from 'src/app/models.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private modelsService: ModelsService,
    private router: Router
  ) {

  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    if (this.modelsService.user) {
      this.router.navigate(['/account']);
    }
  }

}
