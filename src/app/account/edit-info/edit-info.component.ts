import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModelsService } from 'src/app/models.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/msg.service';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';
import { CustomerService } from './customer.service';

@Component({
  selector: 'app-edit-info',
  templateUrl: './edit-info.component.html',
  styleUrls: ['./edit-info.component.css']
})
export class EditInfoComponent implements OnInit {
  form: FormGroup;
  user;

  constructor(
    private fb: FormBuilder,
    private modelsService: ModelsService,
    private customerService: CustomerService,
    private cookieService: CookieService,
    private router: Router,
    private msgService: MsgService) {
    this.user = modelsService.user;
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {

    /* this.form = this.fb.group({
      name: this.user.name,
      phone: this.user.phone
    }); */

    /* this.form = this.fb.group({
      phone: ['', Validators.required],
      name: ['', Validators.required]
    });

    this.form.setValue({
      name: this.user.name,
      phone: this.user.phone
    }); */

    this.form = this.fb.group({
      name: [this.user.name, Validators.required],
      phone: [this.user.phone, Validators.required]
    });
  }

  async submit(post) {
    if (this.form.invalid) {
      return;
    }

    $('#btnEditInfo').attr('disabled', true);
    await this.customerService.update(post)
      .subscribe(response => {
        const { data } = response;
        this.modelsService.set_user_data(data);
        this.msgService.send({
          type: 'setuserdata',
          user_data: data
        });
      }, (error) => {
        $('#btnEditInfo').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnEditInfo').attr('disabled', false);
      });
  }

}
