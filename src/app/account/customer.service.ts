import { Injectable } from '@angular/core';
import { Resolve, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot, Router, RoutesRecognized } from '@angular/router';
import { ModelsService } from '../models.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CustomerService implements Resolve<any> {
  urlSegments: any;

  constructor(
    private httpClient: HttpClient,
    private modelsService: ModelsService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {

  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> {
    // params['id'] route.params.id
    this.router.events.subscribe(data => {
      this.urlSegments = this.location.path();
      // console.log(data, 'data 1....');
      if (data instanceof RoutesRecognized) {
        this.urlSegments = data;
        console.log(this.urlSegments, 'router.url...');
      }
    });

    return this.get(route.queryParams);
  }

  get(query: any = '') {
    const formatQuery = Object.keys(query).length === 0 ? '' : '?' + this.modelsService.query_string(query);
    return this.httpClient.get<any>(`products${formatQuery}`);
  }


}
