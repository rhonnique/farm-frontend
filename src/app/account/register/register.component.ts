import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModelsService } from 'src/app/models.service';
import { CookieService } from 'ngx-cookie-service';
import { MsgService } from 'src/app/msg.service';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private modelsService: ModelsService,
    private cookieService: CookieService,
    private router: Router,
    private msgService: MsgService) {
    this.createRegisterForm();
  }

  ngOnInit() {
  }

  createRegisterForm() {
    this.registerForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      phone: ['', Validators.required],
      name: ['', Validators.required],
    });
  }

  async register(post) {
    $('#btnRegister').attr('disabled', true);
    await this.modelsService.register(post)
      .subscribe(response => {
        const { data, token } = response;
        this.cookieService.delete(environment.tokenName);
        const expiredDate = new Date();
        expiredDate.setDate(expiredDate.getDate() + 30);
        this.cookieService.set(environment.tokenName, token, expiredDate);
        this.modelsService.set_user_data(data);
        this.msgService.send({
          type: 'setuserdata',
          user_data: data
        });
        this.router.navigate(['/account']);
      }, (error) => {
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnRegister').attr('disabled', false);
      });
  }

}
