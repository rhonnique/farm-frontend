import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-menu',
  template: `
  <nav class="navbar navbar-expand-sm bg-light">

    <ul class="navbar-nav">
      <li class="nav-item">
        <a routerLink="/account" class="nav-link" routerLinkActive="active">
        Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a routerLink="order-history" class="nav-link" routerLinkActive="active">
        Order History
        </a>
      </li>
      <li class="nav-item">
        <a routerLink="/whistlist" class="nav-link" routerLinkActive="active">
          Whistlist
        </a>
      </li>
      <li class="nav-item">
        <a routerLink="edit-info" class="nav-link" routerLinkActive="active">
          Edit Account
        </a>
      </li>
      <li class="nav-item">
        <a routerLink="change-password" class="nav-link" routerLinkActive="active">
          Change Password
        </a>
      </li>
    </ul>

</nav>
  `,
  styles: []
})
export class MenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
