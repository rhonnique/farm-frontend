import { Injectable } from '@angular/core';
import { ModelsService } from 'src/app/models.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(
    private httpClient: HttpClient,
    private modelsService: ModelsService,
    private activatedRoute: ActivatedRoute) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> {
    return this.get(route.queryParams);
  }

  get(query: any = '') {
    const formatQuery = Object.keys(query).length === 0 ? '' : '?' + this.modelsService.query_string(query);
    return this.httpClient.get<any>(`products${formatQuery}`);
  }
}
