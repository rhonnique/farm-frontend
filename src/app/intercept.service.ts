import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, throttle, catchError } from 'rxjs/operators';
import { environment } from './../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class InterceptService implements HttpInterceptor {

  constructor(private cookieService: CookieService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
    const sessionCheck: boolean = this.cookieService.check(environment.tokenName);
    if (sessionCheck) {
      const token = this.cookieService.get(environment.tokenName);
      request = request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
    }

    request = request.clone({ url: `${environment.apiUrl}${request.url}` });

    return next.handle(request).timeout(30000);
    /* return next.handle(
      req.clone({
        headers: req.headers.append('Authorization', 'Bearer kjhjjhkjj897879ghg')
      })
    ); */
  }

  /* intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('event--->>>', event);
        }
        return event;
      }));
  } */


}
