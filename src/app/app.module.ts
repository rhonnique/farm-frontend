import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
// import { AccountModule } from './account/account.module';
import { Angular4PaystackModule } from 'angular4-paystack';

import { AppComponent } from './app.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductGetComponent } from './product-get/product-get.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { CookieService } from 'ngx-cookie-service';

import { ModelsService } from './models.service';

import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { InterceptService } from './intercept.service';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './components/category.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './products/products.component';
import { PaginateComponent } from './components/product/paginate/paginate.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { PriceComponent } from './components/price.component';
import { BreadcrumbComponent } from './components/breadcrumb.component';
import { CartsComponent } from './carts/carts.component';
import { LoginComponent } from './account/login/login.component';
import { DashboardComponent } from './account/dashboard/dashboard.component';
import { RegisterComponent } from './account/register/register.component';
import { MenuComponent } from './account/menu.component';
import { ChangePasswordComponent } from './account/change-password/change-password.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OrderHistoryComponent } from './account/order-history/order-history.component';
import { AccountComponent } from './account/account/account.component';
import { ForgotPasswordComponent } from './account/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';
import { EditInfoComponent } from './account/edit-info/edit-info.component';
import { CheckoutComponent } from './checkout/checkout/checkout.component';
import { DeliveryComponent } from './checkout/delivery/delivery.component';
import { PaymentComponent } from './checkout/payment/payment.component';
import { CartSummaryComponent } from './components/cart-summary/cart-summary.component';
import { environment } from 'src/environments/environment.prod';
import { DiscountComponent } from './checkout/discount/discount.component';
import { CompleteComponent } from './checkout/complete/complete.component';


export function modelsServiceFactory(provider: ModelsService) {
  return () => provider.load();
}
@NgModule({
  declarations: [
    AppComponent,
    ProductAddComponent,
    ProductGetComponent,
    ProductEditComponent,
    HomeComponent,
    CategoriesComponent,
    CategoryComponent,
    ProductComponent,
    ProductsComponent,
    PaginateComponent,
    ProductDetailsComponent,
    PriceComponent,
    BreadcrumbComponent,
    CartsComponent,
    LoginComponent,
    RegisterComponent,
    MenuComponent,
    AccountComponent,
    DashboardComponent,
    ChangePasswordComponent,
    OrderHistoryComponent,
    NotFoundComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    EditInfoComponent,
    CheckoutComponent,
    DeliveryComponent,
    PaymentComponent,
    CartSummaryComponent,
    DiscountComponent,
    CompleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // AccountModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    Angular4PaystackModule.forRoot(environment.pk),
  ],
  providers: [
    CookieService,
    ModelsService,
    {
      provide: APP_INITIALIZER,
      useFactory: modelsServiceFactory,
      deps: [ModelsService], multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
