import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../environments/environment';
import { ModelsService } from './models.service';

@Injectable({ providedIn: 'root' })
export class NoAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private cookieService: CookieService,
    private modelsService: ModelsService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const user = this.modelsService.user;
    if (!user) {
      return true;
    }

    this.router.navigate(['/account']);
    return false;
  }
}
