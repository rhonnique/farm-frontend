import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ProductsService } from '../products.service';
import { ModelsService } from '../models.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  getProducts: any[];
  categories: any[] = [];

  constructor(
    private productsService: ProductsService,
    private modelsService: ModelsService
    ) { }

  ngOnInit() {
    this.get_products();
    this.categories = this.modelsService.categories;
    // console.log(this.modelsService.categories, 'categories....');
  }

  get_products() {
    this.productsService.get_rand()
      .subscribe(response => {
        // console.log(response.data.data, 'response....');
        this.getProducts = response.data.data;
        /* this.getProducts = [
          {
            name: 'fggrrtt'
          }
        ]; */
      }, (error) => {
        console.log(error.error, 'errorr....');
      });
  }

}
