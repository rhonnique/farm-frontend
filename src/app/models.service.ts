import { Injectable, Injector } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { environment } from './../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map, throttle, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {
  auth = 1;
  categories: any[] = [];
  user: any;
  sessionId;
  // urlSegments: any;

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
    // private injector: Injector,
    private location: Location
  ) {
    // this.urlSegments = this.location.path();
    this.sessionId = cookieService.get(environment.sessionName);
    const sessionCheck: boolean = this.cookieService.check(environment.sessionName);
    if (sessionCheck === false) {
      this.cookieService.delete(environment.sessionName, '/');
      this.sessionId = this.makeid(32);
      const expiredDate = new Date();
      expiredDate.setDate(expiredDate.getDate() + 30);
      this.cookieService.set(environment.sessionName, this.sessionId, expiredDate, '/');
    }
  }

  load() {
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(`sessions`)
        .subscribe((response): any => {
          // tslint:disable-next-line:quotemark
          // tslint:disable-next-line:no-string-literal
          this.set_data(response['data']);
          resolve(true);
        }, (error) => {
          console.log(error.error, 'errrr....');
        }, () => {
          // const router = this.injector.get(Router);
          // console.log('router.url', this.location.path());
          const tokenCheck: boolean = this.cookieService.check(environment.tokenName);
          if (tokenCheck && (this.user === false || this.user === '' || !this.user)) {
            this.deleteCookie(environment.tokenName);
          }
        });
    });
  }

  set_data(data) {
    this.categories = data.categories;
    this.user = data.user;
  }

  query_string(obj) {
    return Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
  }

  makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  get_session() {
    let sessionId = this.cookieService.get(environment.sessionName);
    const sessionCheck: boolean = this.cookieService.check(environment.sessionName);
    if (sessionCheck === false) {
      this.cookieService.delete(environment.sessionName, '/');
      sessionId = this.makeid(32);
      const expiredDate = new Date();
      expiredDate.setDate(expiredDate.getDate() + 30);
      this.cookieService.set(environment.sessionName, sessionId, expiredDate, '/');
    }
    return sessionId;
  }

  reset_session() {
    this.sessionId = this.cookieService.get(environment.sessionName);
    const sessionCheck: boolean = this.cookieService.check(environment.sessionName);
    if (sessionCheck === false) {
      this.cookieService.delete(environment.sessionName, '/');
      this.sessionId = this.makeid(32);
      const expiredDate = new Date();
      expiredDate.setDate(expiredDate.getDate() + 30);
      this.cookieService.set(environment.sessionName, this.sessionId, expiredDate, '/');
    }
  }

  login(data: any) {
    return this.httpClient.post<any>('customer/login', data);
  }

  register(data: any) {
    return this.httpClient.post<any>('customer', data);
  }

  change_password(data: any) {
    return this.httpClient.post<any>('customer/change-password', data);
  }

  forgot_password(data: any) {
    return this.httpClient.post<any>('customer/forgot-password', data);
  }

  set_user_data(data) {
    this.user = data;
    console.log(this.user, 'set_user_data..');
  }

  deleteCookie(name) {
    // this.cookieService.delete(name, this.location.path());
    this.cookieService.delete(name, '/');
  }

  getCookie(name) {
    this.cookieService.get(name);
  }

  urlSegments() {
    return this.location.path();
  }

}
