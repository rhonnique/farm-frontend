import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartsService {

  constructor(private httpClient: HttpClient) { }

  get(sessionId) {
    return  this.httpClient.get<any>(`carts/${sessionId}`);
  }

  add(data: any) {
    return  this.httpClient.post<any>('carts', data);
  }

  update(data: any, sessionId) {
    return  this.httpClient.post<any>(`carts/update/${sessionId}`, data);
  }

  get_all(sessionId) {
    // return this.httpClient.get<any>(`carts/${sessionId}`).subscribe(response => response);

    return new Promise((resolve, reject) => {
      this.httpClient
        .get(`carts/${sessionId}`)
        .subscribe((response): any => {
          resolve(response);
        }, (error) => {
          console.log(error.error, 'errrr....');
        });
    });
  }

}
