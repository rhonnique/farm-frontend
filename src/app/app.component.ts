import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { I18nPluralPipe } from '@angular/common';
import {
  NavigationCancel,
  Event,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
  RoutesRecognized
} from '@angular/router';
import { CategoriesService } from './categories.service';
import { environment } from './../environments/environment';
import { MsgService } from './msg.service';
import { ProductsService } from './products.service';
import { Subscription } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import * as $ from 'jquery';
import { ModelsService } from './models.service';
import { CartsService } from './carts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy, OnChanges {
  title = 'xApp';
  categories: any[];
  products: [];
  subscription: Subscription;
  cartCount = 0;
  messages: [];
  carts: any[] = [];
  sessionId;
  user = this.modelsService.user;

  constructor(
    private loadingBar: SlimLoadingBarService,
    private router: Router,
    private categoriesService: CategoriesService,
    private productsService: ProductsService,
    private modelsService: ModelsService,
    private cookieService: CookieService,
    private msgService: MsgService,
    private cartsService: CartsService) {
    this.router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });




    this.subscription = this.msgService.get().subscribe(message => {

      if (message) {
        console.log(message, 'message...');
        switch (message.type) {
          /* case 'addtocart':
            const { qty, product_id, qty_type } = message;
            // tslint:disable-next-line:radix
            const setQty = Number.isNaN(parseInt(qty)) ? 1 : qty;

            this.add_to_cart({
              session_id: this.sessionId,
              product_id,
              qty: setQty,
              qty_type
            });
            break; */
          case 'updatecart':
            const { data } = message;
            this.carts = data;
            this.countCart(data);
            console.log(this.modelsService.get_session(), 'get_session...');
            break;
          case 'setuserdata':
            const { user_data } = message;
            this.user = user_data;
            break;
        }
      }
    });


  }

  ngOnInit(): void {
    this.sessionId = this.modelsService.get_session();
    this.categories = this.modelsService.categories;
    this.get_carts();
    // console.log(this.user, 'user..');

    /* this.router.events.subscribe(data => {
      if (data instanceof RoutesRecognized) {
        // console.log(data.state.root.firstChild.children[0].data.default, 'default 22222 ....');
        const isDefault: boolean = data.state.root.firstChild.children[0] && data.state.root.firstChild.children[0].data.default;
        // console.log(isDefault, 'isDefault....');
        if (isDefault) {
          this.msgService.send({
            type: 'checkout_default'
          });
        }
      }
    }); */
  }

  private navigationInterceptor(event: Event): void {
    if (event instanceof NavigationStart) {
      this.loadingBar.start();
    }
    if (event instanceof NavigationEnd) {
      this.loadingBar.complete();
    }
    if (event instanceof NavigationCancel) {
      this.loadingBar.stop();
    }
    if (event instanceof NavigationError) {
      this.loadingBar.stop();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes, 'user changes..');
  }

  async get_carts() {
    await this.cartsService.get(this.sessionId)
      .subscribe(response => {
        const { data } = response;
        this.carts = data;
        this.countCart(data);
      }, (error) => {
        console.log(error.error, 'get_carts error....');
      });
  }

  /* async add_to_cart(post) {
    $('#btnAddToCart').attr('disabled', true);
    await this.cartsService.add(post)
      .subscribe(response => {
        const { data } = response;
        this.carts = data;
        this.countCart(data);
      }, (error) => {
        console.error(error.error, 'add_to_cart error....');
      }, () => {
        $('#btnAddToCart').attr('disabled', false);
      });
  } */

  countCart(data): void {
    const total = data.length > 0 ? data.reduce((a, b) => +a + +b.qty, 0) : 0;
    this.cartCount = total;
  }

  logout() {
    console.log('logout....');
    this.modelsService.deleteCookie(environment.tokenName);
    this.modelsService.set_user_data('');
    this.user = '';
    this.router.navigate(['/account/login']);
  }


  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

}
