import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ModelsService } from './models.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService implements Resolve<any> {
  auth = 1;

  constructor(
    private httpClient: HttpClient,
    private modelsService: ModelsService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> {
    // params['id']
    if (route.params.id) {
      // console.warn(route.params.id, 'resolve..');
      return this.get({ slug: route.params.id });
    } else {
      return this.get(route.queryParams);
    }

  }

  get(query: any = '') {
    console.log(query, 'product query...');
    const formatQuery = Object.keys(query).length === 0 ? '' : '?' + this.modelsService.query_string(query);
    return this.httpClient.get<any>(`products${formatQuery}`);
  }

  get_rand(): Observable<any> {
    return this.httpClient.get<any>('products/rand');
  }


  /* load() {
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(this.api_link)
        .map(res => res)
        .subscribe(response => {
          this.app_data = response['pages'];
          resolve(true);
        });
    });
  } */



}
