import { Component, OnInit, OnDestroy } from '@angular/core';
// import { PaystackOptions } from 'angular4-paystack';
import { Router, ActivatedRoute } from '@angular/router';
import { ModelsService } from 'src/app/models.service';
import { environment } from 'src/environments/environment';
import { CartsService } from 'src/app/carts.service';
import { CookieService } from 'ngx-cookie-service';
import * as $ from 'jquery';
import { CheckoutService } from '../checkout.service';
import { MsgService } from 'src/app/msg.service';
declare var PaystackPop: any;
import * as moment from 'moment';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit, OnDestroy {

  title: string;
  order: any;
  transId = this.modelsService.makeid(10);
  sessionId = this.cookieService.get(environment.sessionName);
  // options: PaystackOptions;

  constructor(
    private route: ActivatedRoute,
    private modelsService: ModelsService,
    private checkoutService: CheckoutService,
    private cartsService: CartsService,
    private msgService: MsgService,
    private cookieService: CookieService,
    private router: Router
  ) { }

  ngOnInit() {
    const wf = document.createElement('script');
    wf.src = 'https://js.paystack.co/v1/inline.js';
    wf.type = 'text/javascript';
    wf.async = true;
    const s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);

    this.route.parent.data.subscribe((data) => {
      this.order = data.checkout.order;
      /* this.options = {
        amount: 50000,
        email: this.modelsService.user.email,
        ref: `${Math.ceil(Math.random() * 10e10)}`
      }; */
    });
  }

  paymentInit() {
    console.log('Payment initialized');
  }

  paymentDone(ref: any) {
    this.title = 'Payment successfull';
    console.log(this.title, ref);
  }

  paymentCancel() {
    console.log('payment failed');
  }

  async payWithPaystack() {
    const resp: any = await this.checkoutService.sum_order();
    const { total, discount, trans_id } = resp;

    /* const cart: any = resp.data;
    if (!cart || cart.length === 0) {
      this.router.navigate(['/carts']);
      return;
    }

    const total = cart.reduce((a, b) => +a + +(b.qty * b.price), 0); */

    try {
      const handler = PaystackPop.setup({
        key: environment.pk,
        email: this.modelsService.user.email,
        amount: ((total - discount) * 100),
        currency: 'NGN',
        ref: `${trans_id}`,
        callback: (response) => {
          console.log(response, 'payWithPaystack...');
          this.submit_order({
            session_id: this.cookieService.get(environment.sessionName),
            trans_id: response.trxref,
            trans_status: response.status,
            trans_message: response.message
          });
        },
        onClose: () => {
          console.log('window closed');
        },
      });
      handler.openIframe();
    } catch (e) {
      console.log(e);
    }
  }

  async submit_order(post) {
    // $('#btnDelivery').attr('disabled', true);
    await this.checkoutService.order(post)
      .subscribe(response => {
        const { trans_id, order_id } = response;
        console.log(response, 'response...');
        this.cookieService.delete(environment.sessionName, '/');
        this.msgService.send({ type: 'updatecart', data: [] });
        // this.modelsService.deleteCookie(environment.sessionName);
        // const expiredDate = new Date();
        this.cookieService.set(environment.tempOrderId, order_id, moment().add(5, 'm').toDate(), '/');
        // this.modelsService.get_session();
        this.router.navigate(['/order-complete']);

        /* this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/order-complete']);
        }); */
      }, (error) => {
        // $('#btnDelivery').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        // $('#btnDelivery').attr('disabled', false);
      });
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy');
    this.cookieService.delete(environment.tempOrderId, '/');
  }

  /*
  message: "Approved"
reference: "4pyy2We5TH"
status: "success"
trans: "302668417"
transaction: "302668417"
trxref: "4pyy2We5TH"
  */

}
