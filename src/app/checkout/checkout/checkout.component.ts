import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, RoutesRecognized, Router } from '@angular/router';
import { CartsComponent } from 'src/app/carts/carts.component';
import { Subscription } from 'rxjs';
import { MsgService } from 'src/app/msg.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit, OnDestroy {
  cart: {};
  default = false;
  subscription: Subscription;
  setDefault = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private msgService: MsgService,
    private location: Location
  ) {

  }

  ngOnInit() {
    this.route.data.subscribe((data) => {
      const segments = this.router.parseUrl(this.location.path()).root.children.primary.segments;
      const checkoutDefault = segments && segments[1];
      if (checkoutDefault === undefined) {
        this.setDefault = true;
      } else {
        this.setDefault = false;
      }

      this.cart = {
        data: data.checkout.carts,
        order: data.checkout.order,
        set_default: this.setDefault
      };
    });



    this.route.url.subscribe((data) => {

      // console.log(checkoutDefault, 'checkoutDefault...');


      // console.log(this.router.parseUrl(this.location.path()).root.children.primary.segments, 'data.....');
      // console.log(data, 'data.....');
      // this.carts = data.checkout.carts;
    });

    /* this.router.subscribe(data => {
    console.log(this.router.url, 'router.....');
    }); */

    //  console.log(this.route.snapshot.firstChild.url, 'snapshot 2222...');
    // console.log(this.route.snapshot.firstChild.url[0].path, 'snapshot...');



    /* this.router.events.subscribe(data => {
      if (data instanceof RoutesRecognized) {
        console.log(data.state.root.firstChild.children[0].data.default, 'default 22222 ....');
      }
    }); */

    this.subscription = this.msgService.get().subscribe(message => {
      console.log(message, 'checkout_default.....');

      if (message && message.type === 'checkout_default') {
        console.log(message.type, 'checkout_default.....');
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
