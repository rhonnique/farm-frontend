import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, RoutesRecognized } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ModelsService } from 'src/app/models.service';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class OrderSummaryService {

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
    private modelsService: ModelsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> {
    return this.get();
  }

  get() {
    const sessionId = this.cookieService.get(environment.tempOrderId);
    return this.httpClient.get<any>(`checkout/complete/${sessionId}`);
  }
}
