import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModelsService } from 'src/app/models.service';
import { DeliveryService } from './delivery.service';
import * as $ from 'jquery';
import { CheckoutService } from '../checkout.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.css']
})
export class DeliveryComponent implements OnInit {
  form: FormGroup;
  states: any[];
  centers: any[];
  order: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private modelsService: ModelsService,
    private deliveryService: DeliveryService,
    private checkoutService: CheckoutService,
  ) {
    this.form = this.fb.group({
      state: ['', Validators.required],
      pickup_center: ['', Validators.required],
    });
  }

  async ngOnInit() {
    this.route.parent.data.subscribe((data) => {
      this.states = data.checkout.states;
      this.order = data.checkout.order;
    });

    if (this.order) {
      const status = JSON.parse(this.order.status);
      // if (status.delivery.hasOwnProperty('state') && status.delivery.hasOwnProperty('pickup_center')) {
      const delivery = status.hasOwnProperty('delivery') && status.delivery;
      if (delivery && (delivery.hasOwnProperty('state'))) {
        await this.getPickupCenter({ target: { value: delivery.state } });
        this.form.patchValue({
          state: delivery.state,
          pickup_center: delivery.hasOwnProperty('pickup_center') ? delivery.pickup_center : ''
        });
      }
    }



  }

  async getPickupCenter(event) {
    const id = event.target.value;
    if (id === '') {
      this.centers = [];
      this.form.patchValue({ pickup_center: '' });
      return;
    }
    $('#state, #pickup_center, #btnDelivery').attr('disabled', true);
    await this.deliveryService.pickup_centers(id)
      .subscribe(response => {
        const { data } = response;
        this.centers = data;
      }, (error) => {
        $('#state, #pickup_center, #btnDelivery').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        $('#state, #pickup_center, #btnDelivery').attr('disabled', false);
      });
  }

  async submit(post) {
    console.error(post, 'register error....');
    $('#btnDelivery').attr('disabled', true);
    await this.checkoutService.delivery(post)
      .subscribe(response => {
        this.router.navigate(['/checkout/payment']);
      }, (error) => {
        $('#btnDelivery').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnDelivery').attr('disabled', false);
      });
  }

}
