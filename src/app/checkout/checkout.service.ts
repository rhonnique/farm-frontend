import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, RoutesRecognized } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ModelsService } from 'src/app/models.service';
import { environment } from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
    private modelsService: ModelsService
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> {
    return this.get();
  }

  get() {
    const sessionId = this.cookieService.get(environment.sessionName);
    console.log(sessionId, 'sessionId....');
    return this.httpClient.get<any>(`checkout/${sessionId}`);
  }

  update(post) {
    const sessionId = this.cookieService.get(environment.sessionName);
    // tslint:disable-next-line:no-string-literal
    post['session_id'] = sessionId;
    return this.httpClient.post<any>(`checkout/update`, post);
  }

  delivery(post) {
    const sessionId = this.cookieService.get(environment.sessionName);
    // tslint:disable-next-line:no-string-literal
    post['session_id'] = sessionId;
    return this.httpClient.post<any>(`checkout/update`, post);
  }

  order(post) {
    return this.httpClient.post<any>(`checkout/order`, post);
  }

  discount(post) {
    const sessionId = this.cookieService.get(environment.sessionName);
    // tslint:disable-next-line:no-string-literal
    post['session_id'] = sessionId;
    return this.httpClient.post<any>(`checkout/apply-discount`, post);
  }

  sum_order() {
    const sessionId = this.cookieService.get(environment.sessionName);
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(`checkout/sum-order/${sessionId}`)
        .subscribe((response): any => {
          resolve(response);
        }, (error) => {
          console.log(error.error, 'errrr....');
        });
    });
  }

}
