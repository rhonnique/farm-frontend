import { Component, OnInit } from '@angular/core';
import { ModelsService } from '../models.service';
import { CartsService } from '../carts.service';
import * as $ from 'jquery';
import { MsgService } from '../msg.service';

@Component({
  selector: 'app-carts',
  templateUrl: './carts.component.html',
  styleUrls: ['./carts.component.css']
})
export class CartsComponent implements OnInit {
  sessionId: string = this.modelsService.get_session();
  carts: any[] = [];
  cartsUpdate: any[] = [];
  breadcrumbs: any[];
  subTotal = 0;
  discountTotal = 0;
  total = 0;
  discountCode;

  constructor(
    private modelsService: ModelsService,
    private cartsService: CartsService,
    private msgService: MsgService
  ) {
    this.breadcrumbs = [{ name: 'Carts', current: true }];
  }

  ngOnInit() {
    this.get_carts();
  }

  async get_carts() {
    await this.cartsService.get(this.sessionId)
      .subscribe(response => {
        const { data } = response;
        this.carts = data;
        this.sumCart(data);
      }, (error) => {
        console.log(error.error, 'get_carts error....');
      });
  }

  updateQty(id, event) {
    const qty = event.target.value;
    const foundIndex = this.cartsUpdate.findIndex(x => x.id === id);
    if (foundIndex < 0) {
      this.cartsUpdate.push({ id, qty });
    } else {
      this.cartsUpdate[foundIndex].qty = qty;
    }
    console.log(this.cartsUpdate, 'cartsUpdate...');
  }


  async updateCart() {
    $('#btnUpdateCart').attr('disabled', true);
    await this.cartsService.update(this.cartsUpdate, this.sessionId)
      .subscribe(response => {
        const { data } = response;
        this.carts = data;
        this.sumCart(data);
        this.msgService.send({
          type: 'updatecart',
          data
        });
      }, (error) => {
        console.error(error.error, 'add_to_cart error....');
      }, () => {
        this.cartsUpdate = [];
        $('#btnUpdateCart').attr('disabled', false);
      });
  }

  async deleteCart(id) {
    await this.updateQty(id, { target: { value: 0 } });
    this.updateCart();
  }

  sumCart(cart) {
    // console.log(cart, 'sumCart...');
    const CartTotal = cart.reduce((a, b) => +a + +(b.qty * b.price), 0);
    this.subTotal = CartTotal;
    this.total = CartTotal + this.discountTotal;
  }

}
