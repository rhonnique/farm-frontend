import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModelsService } from '../models.service';
import { MsgService } from '../msg.service';
import * as $ from 'jquery';
import { CartsService } from '../carts.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product: {};
  breadcrumbs: any[];
  qty = 1;
  sessionId;

  constructor(
    private modelsService: ModelsService,
    private route: ActivatedRoute,
    private router: Router,
    private cartsService: CartsService,
    private msgService: MsgService) {
    this.route.params.subscribe(params => {
      // params['id']
    });

    this.route.data.subscribe((data: any) => {
      this.product = data.products.data;
      this.breadcrumbs = [
        {
          name: data.products.data.category_name,
          link: `/products`,
          queryString: { category: data.products.data.category_slug }
        },
        {
          name: data.products.data.name,
          current: true
        }
      ];
      // console.log(this.breadcrumbs, 'prod breadcrumbs...');
    });

  }

  ngOnInit() {
    this.sessionId = this.modelsService.get_session();
  }

  async addToCart(post) {
    // console.log(data, this.qty);
    /* this.msgService.send({
      type: 'addtocart',
      qty: this.qty,
      product_id: post.product_id,
      qty_type: post.qty_type,
    }); */

    // tslint:disable-next-line:radix
    const setQty = Number.isNaN(parseInt(post.qty_type)) ? 1 : post.qty_type;
    $('#btnAddToCart').attr('disabled', true);
    await this.cartsService.add({
      qty: this.qty,
      product_id: post.product_id,
      qty_type: setQty,
      session_id: this.sessionId
    })
      .subscribe(response => {
        const { data } = response;
        this.msgService.send({
          type: 'updatecart',
          data
        });
      }, (error) => {
        console.error(error.error, 'add_to_cart error....');
      }, () => {
        $('#btnAddToCart').attr('disabled', false);
      });
  }

}
