import { Component, OnInit } from '@angular/core';
import { ModelsService } from '../models.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: any[];

  constructor(
    private modelsService: ModelsService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe((data: any) => {
      console.log(data.products.data, 'data...');
      this.products = data.products.data.data;
    });
  }

}
