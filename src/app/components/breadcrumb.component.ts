import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  template: `
  <nav *ngIf="breadcrumbs" aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a routerLink="/">Home</a></li>
    <li [ngClass]="['breadcrumb-item', breadcrumb.current ? 'active' : '']" *ngFor="let breadcrumb of breadcrumbs">
        <a *ngIf="breadcrumb.link && breadcrumb.queryString"
        routerLink="/{{breadcrumb.link}}" [queryParams]="breadcrumb.queryString">{{breadcrumb.name}}</a>

        <a *ngIf="breadcrumb.link && !breadcrumb.queryString" routerLink="/{{breadcrumb.link}}">{{breadcrumb.name}}</a>

        <span *ngIf="breadcrumb.current">{{breadcrumb.name}}</span>
    </li>
  </ol>
</nav>
  `,
  styles: []
})
export class BreadcrumbComponent implements OnInit {
  @Input() breadcrumb: any;
  breadcrumbs: any[];
  constructor() { }

  ngOnInit() {
    this.breadcrumbs = this.breadcrumb;
    // console.log(this.breadcrumbs, 'data...');
  }

}
