import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-price',
  template: `{{ price | number : '1.2-2':'en' }}`,
  styles: []
})
export class PriceComponent implements OnInit {
  @Input() price: string;
  constructor() { }

  ngOnInit() {
  }

}
