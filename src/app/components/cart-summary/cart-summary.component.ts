import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as $ from 'jquery';
import { CheckoutService } from 'src/app/checkout/checkout.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart-summary',
  templateUrl: './cart-summary.component.html',
  styles: []
})
export class CartSummaryComponent implements OnInit {
  @Input() cart: any;
  carts: [];
  subTotal = 0;
  discountTotal = 0;
  total = 0;
  orderComment;
  setDefault = false;
  // tslint:disable-next-line:variable-name
  form_discount: FormGroup;

  constructor(
    private fb: FormBuilder,
    private checkoutService: CheckoutService,
    private router: Router
  ) { }

  ngOnInit() {
    console.log(this.cart, 'this.cart....');
    const { data, set_default, order } = this.cart;
    this.carts = data;
    let getDiscount = JSON.parse(order.status);
    getDiscount = getDiscount.discount ? getDiscount.discount.amount : 0;
    this.sumCart(data, getDiscount);
    this.setDefault = set_default;

    this.form_discount = this.fb.group({
      discount_code: ['', Validators.required],
      comment: ['', Validators.required],
    });
  }

  sumCart(cart, discount = 0) {
    console.log(cart, 'sumCart...');
    const CartTotal = cart.reduce((a, b) => +a + +(b.qty * b.price), 0);
    this.subTotal = CartTotal;
    this.discountTotal = discount;
    this.total = CartTotal - discount;
  }

  async submit_discount(post) {

    $('#btnApplyDiscount').attr('disabled', true);
    await this.checkoutService.discount(post)
      .subscribe(response => {
        // this.router.navigate(['/checkout/delivery']);
      }, (error) => {
        $('#btnApplyDiscount').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnApplyDiscount').attr('disabled', false);
      });
  }

  async submit_continue() {
    const post = { comment: this.orderComment };
    $('#btnContinue').attr('disabled', true);
    await this.checkoutService.update(post)
      .subscribe(response => {
        this.router.navigate(['/checkout/delivery']);
      }, (error) => {
        $('#btnContinue').attr('disabled', false);
        console.error(error.error, 'register error....');
      }, () => {
        $('#btnContinue').attr('disabled', false);
      });
  }

}
