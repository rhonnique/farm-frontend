import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnChanges {
  @Input() products: [];

  constructor() {}

  ngOnInit() {
    console.log(this.products);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.products) {
      console.log(this.products, 'changes..');
    }
  }

}
