import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { MsgService } from 'src/app/msg.service';

@Component({
  selector: 'app-category',
  template: `
  <a *ngFor="let category of categories; let i= index"
  routerLink="/products" [queryParams]="{category: category.slug }">
  {{category.name}}
  </a>`,
  styles: []
})
export class CategoryComponent implements OnInit, OnChanges {
  @Input() categories: [];

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {}

}
