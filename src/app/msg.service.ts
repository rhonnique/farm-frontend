import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MsgService {
  private subject = new Subject<any>();

  constructor() { }

  send(message: any) {
    this.subject.next(message);
  }

  clear() {
    this.subject.next();
  }

  get(): Observable<any> {
    return this.subject.asObservable();
  }

}
